<?php

use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= Yii::$app->name ?></h1>

    </div>

    <div class="body-content">

        <div class="row">
            
            <?php
                foreach($datos as $registro){
            ?>
            <div class="col-lg-6">
              <div class="thumbnail">
                 <?= Html::img("@web/imgs/$registro->foto") ?>
                 <div class="caption">
                <h3><?= Html::encode($registro->titulo) ?></h3>
                <p><?= Html::encode($registro->texto) ?></p>
                </div>
              </div>    
            </div>
            <?php 
                }
            ?>
        </div>

    </div>
</div>
