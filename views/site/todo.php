<?php
 use yii\grid\GridView;
 use yii\helpers\Html;
?>
<h3>Noticias</h3>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=>'Mostrando {begin}-{end} de {totalCount} noticias',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'titulo',
            'texto',
            [
                'attribute' => 'foto',
                 'format' => 'html',    
                 'value' => function ($data) {
                 return Html::img(Yii::getAlias('@web').'/imgs/'. $data['foto'],
                ['width' => '70px']);
            },
            ],
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

<h3>Artículos</h3>
<?= GridView::widget([
        'dataProvider' => $dataProvider2,
        'summary'=>'Mostrando {begin}-{end} de {totalCount} artículos',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'titulo',
            'texto',
            [
                'attribute' => 'foto',
                 'format' => 'html',    
                 'value' => function ($data) {
                 return Html::img(Yii::getAlias('@web').'/imgs/'. $data['foto'],
                ['width' => '70px']);
            },
            ],
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

