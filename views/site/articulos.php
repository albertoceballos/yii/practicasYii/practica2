<?php
use yii\helpers\Html;
?>
<div class="row row-flex row-flex-wrap">
    <?php 
        foreach($datos as $registro){
    ?>
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <?=   Html::img("@web/imgs/$registro->foto") ?>
      <div class="caption">
          <h3><?= Html::encode($registro->titulo) ?></h3>
          <p><?= Html::encode($registro->texto) ?></p>
        <p><?= Html::a('Leer más', ['site/artcompleto', 'id' => $registro->id]) ?></p>
      </div>
    </div>
  </div>
    <?php
      }
    ?>
</div>

