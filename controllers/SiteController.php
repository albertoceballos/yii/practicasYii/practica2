<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Noticias;
use app\models\Articulos;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $datos= Noticias::find()->all();
        return $this->render('index',['datos'=>$datos]);
    }

 
    public function actionArticulos()
    {
        $articulos=new Articulos;
        $datos=$articulos::find()->all();
        return $this->render('articulos',['datos'=>$datos]);
    }
    
    public function actionTodo(){
        
         $dataProvider=new \yii\data\ActiveDataProvider([
          'query'=> Noticias::find(),
          'pagination' => [ 'pageSize' => 2 ,
            'pageParam'=>'d1',
             ],
              'sort'=>[
                  'sortParam'=>'d1',
             ]
        ]);
        
         $dataProvider2=new \yii\data\ActiveDataProvider([
          'query'=> Articulos::find(),
          'pagination' => [ 'pageSize' => 1,
             'pageParam'=>'d2',
             ],
             'sort'=>[
                  'sortParam'=>'d2',
             ]
          ]); 
        return $this->render('todo',['dataProvider'=>$dataProvider,'dataProvider2'=>$dataProvider2]);
    }
    
    public function actionArtcompleto($id){
         $comando=Yii::$app->db->createCommand("Select * from articulos where id=$id");
        $datos=$comando->queryAll();
        return $this->render('articuloCompleto',['datos'=>$datos]);
    }
  
}
